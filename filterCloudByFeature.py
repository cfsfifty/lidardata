import numpy      as np
import cloudComPy as cc                                              # import the CloudComPy module
cc.initCC()                                                          # to do once before dealing with plugin

mesh  = cc.loadMesh("scene_mesh_decimated_textured.obj")             # read a mesh from a file
cloud = mesh.getAssociatedCloud()                                    # mesh points
#cloud = cc.loadPointCloud("cloud.xyz")
bbox  = cloud.getOwnBB()
print("cloud bbox: %s, %s" % (bbox.minCorner(), bbox.maxCorner()))

## compute feature values to new scalar field
# radius = cc.GetPointCloudRadius([cloud], 20)
radius = 0.5
cc.computeFeature(cc.GeomFeature.EigenValue3, radius, [cloud]) 
print("\nsphere-radius=%f" % (radius))
nsf = cloud.getNumberOfScalarFields()
sfR = cloud.getScalarField(nsf-1)
#asfR= sfR.toNpArray(); print("\nsize=", asfR.size)                  # scalar field as a numpy array
sfR.computeMinAndMax()
#sfR.computeMeanAndVariance()
minR = sfR.getMin()
maxR = sfR.getMax()
print("\nmin-feature-value=%f, max=%f" % (minR, maxR))
cloud.setCurrentOutScalarField(nsf-1)                                # active/displayed scalar field: nsf

## filter cloud by active scalar field
filteredCloud = cc.filterBySFValue(float(0.0), float(0.01), cloud)   # keep only the points with feature value in bounds
nsf = filteredCloud.getNumberOfScalarFields()
sf1 = filteredCloud.getScalarField(nsf-1)
sf1.setValue(0, maxR)   # set field value point[0] to old max, so that scalarfield domain does not change (same coloring)!

res=cc.SavePointCloud(filteredCloud, "filteredCloud.bin")            # save the filtered cloud to a file
